
# Castor Project - Script d'Automatisation

Ce projet utilise Castor pour automatiser diverses tâches courantes dans le développement de projets Symfony et la gestion de l'environnement. Le fichier `castor.php` contient plusieurs fonctions pour installer des outils, initialiser des projets et configurer des environnements.

## Prérequis

- PHP 8.3 ou supérieur
- Composer
- Apache avec les modules `mod_ssl` et `mod_rewrite` activés

## Installation

Récupérer la dernière version de l'exécutable dans les releases et installer le dans le $PATH

```bash
sudo mv CPAMG.linux.x86_64 /usr/local/bin/cpamg
```

## Commandes disponibles

### 1. Installer la CLI Symfony

Installe la CLI Symfony en ajoutant le dépôt nécessaire et en installant le paquet `symfony-cli`.

```bash
cpamg install_symfony_cli
```

### 2. Installer NVM (Node Version Manager)

Installe NVM en téléchargeant le script d'installation officiel et en l'exécutant.

```bash
cpamg install_nvm
```

### 3. Initialiser un projet Symfony

Initialise un projet Symfony en spécifiant le chemin, la version et le type (`skeleton` par défaut ou `webapp`).

```bash
cpamg init_symfony <path> <version> [type]
```

#### Exemple

```bash
cpamg init_symfony /chemin/vers/projet 5.4 webapp
```

### 4. Initialiser un projet Symfony en mode webapp avec Webpack Encore et TailwindCSS

Initialise un projet Symfony en mode webapp, installe Webpack Encore et configure TailwindCSS.

```bash
cpamg init_symfony_tailwindcss <path> <version>
```

#### Exemple

```bash
cpamg init_symfony_tailwindcss /chemin/vers/projet 5.4
```

### 5. Créer un Vhost en HTTPS et l'activer

Crée un Virtual Host en HTTPS pour le projet spécifié, en configurant le fichier de configuration Apache et en ajoutant les permissions nécessaires.

```bash
cpamg init_https <path>
```

#### Exemple

```bash
cpamg init_https /chemin/vers/projet
```

### Notes

- Assurez-vous d'avoir les permissions nécessaires pour exécuter des commandes `sudo` lors de la création du Vhost et de l'installation des paquets.
- La fonction `init_https` ajoutera une entrée dans le fichier `/etc/hosts` pour un accès local via `127.0.0.1`.

## Dépannage

### Erreur 403 lors de l'accès au Vhost

Si vous rencontrez une erreur 403 lors de l'accès au Vhost, assurez-vous que les permissions sur le répertoire du projet et ses répertoires parents sont correctement définies pour permettre à Apache d'y accéder.

```bash
sudo chown -R www-data:www-data /chemin/vers/projet
sudo find /chemin/vers/projet -type d -exec chmod 755 {} \;
sudo find /chemin/vers/projet -type f -exec chmod 644 {} \;
```

## Contact

Pour toute question ou problème, n'hésitez pas à ouvrir une issue sur le dépôt ou à me contacter par email.
