<?php

use Castor\Attribute\AsTask;

use function Castor\io;
use function Castor\capture;

#[AsTask(description: 'Installer la CLI Symfony')]
function install_symfony_cli(): void
{
    io()->title('Installation de la CLI Symfony');
    io()->info(capture("curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.deb.sh' | sudo -E bash"));
    io()->info(capture('sudo apt install -y symfony-cli'));
    io()->success('CLI Symfony installée avec succès!');
}

#[AsTask(description: 'Installer NVM (Node Version Manager)')]
function install_nvm(): void
{
    io()->title('Installation de NVM');
    $installScript = 'curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash';
    io()->info(capture($installScript));

    io()->success('NVM installé, Merci de faire un source de votre fichier bashrc');
}

#[AsTask(description: 'Initialise un projet Symfony')]
function init_symfony(string $path, string $version, string $type = "skeleton"): void
{
    io()->title('Initialisation du projet Symfony');

    $command = sprintf('symfony new %s --version=%s', $path, $version);

    if ($type === 'webapp') {
        $command .= ' --webapp';
    }

    io()->info(capture($command));
    io()->success('Projet Symfony initialisé!');
}

#[AsTask(description: 'Initialise un projet Symfony en mode webapp avec Webpack Encore et TailwindCSS')]
function init_symfony_tailwindcss(string $path, string $version): void
{
    io()->title('Initialisation du projet Symfony en mode webapp');

    io()->info(capture(sprintf('symfony new %s --version=%s --webapp', $path, $version)));

    $projectPath = realpath($path);
    io()->info(capture(sprintf('cd %s && composer require symfony/webpack-encore-bundle', $projectPath)));

    io()->info(capture(sprintf('cd %s && yarn add tailwindcss@latest postcss@latest autoprefixer@latest', $projectPath)));
    io()->info(capture(sprintf('cd %s && npx tailwindcss init -p', $projectPath)));

    file_put_contents($projectPath . '/tailwind.config.js', 'module.exports = {
        content: [
            "./templates/**/*.html.twig",
            "./assets/**/*.js"
        ],
        theme: {
            extend: {},
        },
        plugins: [],
    };');

    file_put_contents($projectPath . '/assets/app.css', '@tailwind base;
@tailwind components;
@tailwind utilities;');

    io()->success('Projet Symfony en mode webapp avec Webpack Encore et TailwindCSS initialisé!');
}

#[AsTask(description: 'Crée un Vhost en HTTPS et l\'active')]
function init_https(string $path): void
{
    io()->title('Création d\'un Vhost en HTTPS');

    $projectName = basename(realpath($path));
    $domainName = sprintf('%s.cnamts.local', $projectName);
    $tempVhostPath = sprintf('/tmp/%s.conf', $domainName);
    $vhostPath = sprintf('/etc/apache2/sites-available/%s.conf', $domainName);

    $vhostConfig = sprintf('
<VirtualHost *:443>
    ServerName %s
    DocumentRoot %s/public

    <Directory %s/public>
        AllowOverride All
        Order Allow,Deny
        Allow from All
        Require all granted
    </Directory>

    SSLEngine on
    SSLCertificateFile /etc/apache2/ssl/_wildcard.dev.local+4.pem
    SSLCertificateKeyFile /etc/apache2/ssl/_wildcard.dev.local+4-key.pem

    ErrorLog ${APACHE_LOG_DIR}/%s_error.log
    CustomLog ${APACHE_LOG_DIR}/%s_access.log combined
</VirtualHost>', $domainName, $path, $path, $projectName, $projectName);

    file_put_contents($tempVhostPath, $vhostConfig);

    io()->info(capture(sprintf('sudo mv %s %s', $tempVhostPath, $vhostPath)));

    io()->info(capture(sprintf('sudo a2ensite %s.conf', $domainName)));
    io()->info(capture('sudo systemctl reload apache2'));

    //$hostsEntry = sprintf('127.0.0.1 %s', $domainName);
    //file_put_contents('/etc/hosts', $hostsEntry . PHP_EOL, FILE_APPEND);

    io()->info(capture(sprintf('sudo chown -R www-data:www-data %s', $path)));
    io()->info(capture(sprintf('sudo find %s -type d -exec chmod 755 {} \\;', $path)));
    io()->info(capture(sprintf('sudo find %s -type f -exec chmod 644 {} \\;', $path)));

    $parentPath = dirname(realpath($path));
    while ($parentPath != '/' && $parentPath != '.' && $parentPath != '') {
        io()->info(capture(sprintf('sudo chmod 755 %s', $parentPath)));
        $parentPath = dirname($parentPath);
    }

    io()->success('Vhost en HTTPS créé et activé!');
    io()->note(sprintf('Ajouter l\'entrée suivante dans le fichier hosts de Windows: 127.0.0.1 %s', $domainName));
}
